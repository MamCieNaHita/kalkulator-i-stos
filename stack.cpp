#include "stack.hpp"
void stack::sizeup(void)            //Funkcja zwiekszajaca rozmiar stosu o 1
{
    int *newtab=new int[size+1];
    for(int i=0; i<size; i++)
    {
	newtab[i]=stacktab[i];
    }
    delete[] stacktab;
    stacktab=newtab;
    size++;
    return;
}
void stack::sizedown(void)            //Funkcja zmniejszajaca rozmiar stosu o 1
{
    if(size<1)
    {
        cout << "Probowano zmniejszyc pusty stos!\n";
	ok=false;
        return;
    }
    int *newtab=new int[size-1];
    for(int i=0; i<size-1; i++)        
    {
        newtab[i]=stacktab[i];
    }
    delete[] stacktab;
    stacktab=newtab;
    size--;
    return;
}
stack::stack(void)                    //Konstruktor, tworzy stos z 1 pozycja
{
    size=0;
    stacktab=new int[0];
    ok=true;
}
stack::~stack(void)                    //Destruktor, usuwa caly stos
{
    delete[] stacktab;
}
void stack::push(int element)        //Funkcja umieszczajaca element na stosie
{
    sizeup();
    stacktab[size-1]=element;
    return;
}
void stack::pop(void)                //Funkcja usuwajaca element ze szczytu stosu
{
    sizedown();
    return;
}
int stack::top(void)                //Funkcja zwracajaca element ze szczytu stosu bez usuwania go
{
    if(size<1)
    {
        cout << "Nie ma czego zwrocic z pustego stosu!";
	ok=false;
        return 0;
    }
    return stacktab[size-1];
}
int stack::isempty(void)            //Funkcja zwracajaca 1 gdy stos jest pusty
{
    if(size<1)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
int stack::size(void)                //Funkcja zwracajaca aktualny rozmiar stosu
{
    return size;
}