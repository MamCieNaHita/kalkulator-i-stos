#ifndef CALC
#define CALC
#include <iostream>
#include "stack.hpp"
using namespace std;
class calc{
public:
    int add(void);		//Funkcja realizujaca postfixowe dodawanie, zwraca wynik z dodawania i umieszcza go na stosie.
    int subtract(void);		//Funkcja realizujaca postfixowe odejmowanie, zwraca wynik z odejmowania i umieszcza go na stosie.
    int multiply(void);		//Funkcja realizujaca postfixowe mnozenie, zwraca wynik z mnozenia i umieszcza go na stosie.
    int work(void);		//Funkcja, ktora wczytuje dane do stosu, przeprowadza na nim operacje oraz wypisuje wynik. Zwraca -1 w przypadku bledu, 0 w przypadku bezproblemowego dzialania.
private:
    stack mystack;
};
#endif