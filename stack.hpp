#ifndef STACK
#define STACK
#include <iostream>
using namespace std;
class stack{
    friend class calc;
public:
    void sizeup(void);		//Zwieksza rozmiar stosu o 1
    void sizedown(void);	//Zmniejsza rozmiar stosu o 1
    stack(void);		//Konstruktor, tworzy stos o 0-wym rozmiarze
    ~stack(void);		//Destruktor
    void push(int);		//Funkcja umieszczajaca element na stosie
    void pop(void);		//Funkcja usuwajaca element ze stosu
    int top(void);		//Funkcja zwracajaca element ze szczytu stosu
    int isempty(void);		//Zwraca 1 gdy stos jest pusty
    int size(void);		//Funkcja zwracajaca rozmiar stosu
private:
    int size;			//Zmienna przechowujaca rozmiar stosu
    int *stacktab;		//Dynamiczna tablica, na ktorej umieszczane sa liczby
    bool ok;			//Zmienna boolean, ktora jest ustawiana na false w przypadku niepowodzenia na ktoryms z etapow liczenia
};
#endif