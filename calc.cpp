#include "calc.hpp"
#include <iostream>
#include <cctype>
using namespace std;
int calc::add(void)
{

    int tempx=mystack.top();
    mystack.pop();
    int tempy=mystack.top();
    mystack.pop();
    mystack.push(tempx+tempy);
    return tempx+tempy;
}
int calc::subtract(void)
{
    int tempx=mystack.top();
    mystack.pop();
    int tempy=mystack.top();
    mystack.pop();
    mystack.push(tempx-tempy);
    return tempx-tempy;
}
int calc::multiply(void)
{
    int tempx=mystack.top();
    mystack.pop();
    int tempy=mystack.top();
    mystack.pop();
    mystack.push(tempx*tempy);
    return tempx*tempy;
}
int calc::work(void)
{
    char c;
    int temp;
    while(c=cin.peek())
    {
	if(isdigit(c))
	{
	    cin>>temp;
	    mystack.push(temp);
	}
	if(c=='+')
	{
	    add();
	    cin.get();
	}
	if(c=='-')
	{
	    subtract();
	    cin.get();
	}
	if(c=='*')
	{
	    multiply();
	    cin.get();
	}
	if(c==' ' || c=='\n')
	{
	    cin.get();
	    if(c=='\n')
	    {
		break;
	    }
	}
	if(!(isdigit(c) || c=='+' || c=='-' || c=='*' || c==' ' || c=='\n'))
	{
	    cout<<"Blad przy wprowadzaniu"<<endl;
	    return -1;
	}
    }
    if(mystack.ok && mystack.showsize()==1)
    {
	cout<<"Wynik to: "<< mystack.stacktab[0] << endl;
	mystack.sizedown();
	return 0;
    }
    else
    {
	cout<<"Blad"<<endl;
	delete [] mystack.tab;
	mystack.tab=new int[0];
	mystack.size=0;
	return -1;
    }
}
