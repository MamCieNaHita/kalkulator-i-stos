#include <iostream>
#include "stack.hpp"
#include "calc.hpp"
using namespace std;
int main()
{
    calc calculator;
    char choose='0';
    while(true)
    {
	if(choose!='q' && choose!='w')
	{
	    cout<<"Wpisz q aby wyjsc, wpisz w aby pracowac"<<endl;
	    cin>>choose;
	    cin.ignore(1000, '\n');
	}
	if(choose=='q')
	{
	    return 0;
	}
	if(choose=='w')
	{
	    calculator.work();
	    choose='0';
	}
    }
}